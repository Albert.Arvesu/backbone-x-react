import React, { useReducer } from 'react'

import { Container, Button, Actions } from './Counter.styled'

const initialState = {count: 0};

const reducer = (state, action) => {
  switch (action.type) {
    case 'increment':
      return {count: state.count + 1};
    case 'decrement':
      return {count: state.count - 1};
    default:
      throw new Error();
  }
}

const Counter = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <Container>
      Count: {state.count}
      <Actions>
        <Button onClick={() => dispatch({type: 'decrement'})}>-</Button>
        <Button onClick={() => dispatch({type: 'increment'})}>+</Button>
      </Actions>
    </Container>
  );
}

export default Counter