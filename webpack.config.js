const path = require('path');

module.exports = {
  entry: path.resolve(__dirname, './react/index.js'),
  mode: 'development',
  devtool: 'source-map',
  resolve: {
      extensions: ['*', '.js']
  },
  output: {
    clean: false,
    path: path.resolve(__dirname, './dist'),
    filename: 'app.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader:'babel-loader'
        }
      }
    ]
  },
};