import styled from 'styled-components'

export const Container = styled.div`
  padding: 2rem;
  margin: auto;
`

export const Actions = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`

export const Button = styled.button`
  border: 0;
  color: white;
  border-radius: 4px;
  background-color: #5bc0de;
  border-color: #46b8da;
  width: 50px;
  margin-left: 2px;
`