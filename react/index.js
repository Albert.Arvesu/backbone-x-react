import ReactDOM from 'react-dom';
import React from 'react';

import Counter from './Counter'

window.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(<Counter />, document.getElementById('app'));
});
